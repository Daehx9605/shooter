﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    private Move move;

    private void Awake()
    {
        move = GetComponent<Move>();
    }
    public void PlayerRotation()
    {
        if (move.getCanmove == false)
        {
            transform.Rotate(Vector3.right*90,Space.Self);
            transform.position += (transform.forward * 0.2f);
        }
    }
}
