﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    private Checker checker;
    private bool canMove;
    private Rotation rotation;

    private void Awake()
    {
        checker = GetComponent<Checker>();
        rotation = GetComponent<Rotation>();
    }
    private void FixedUpdate()
    {
        checker.Check();
        if (canMove)
        {
            float x = Input.GetAxis("Horizontal") * Time.deltaTime * 1f;
            float y = Input.GetAxis("Vertical") * Time.deltaTime * 1f;
            //if (Input.GetKey(KeyCode.UpArrow))
            //{
            //    transform.rotation = Quaternion.LookRotation(0, 0, 0);
            //}
            //if (Input.GetKey(KeyCode.DownArrow))
            //{
            //    transform.rotation = Quaternion.Euler(0, 180, 0);
            //}
            //if (Input.GetKey(KeyCode.RightArrow))
            //{
            //    transform.rotation = Quaternion.Euler(0, 90, 0);
            //}
            //if (Input.GetKey(KeyCode.LeftArrow))
            //{
            //    transform.rotation = Quaternion.Euler(0, -90, 0);
            //}
            //transform.rotation = Quaternion.LookRotation(transform.forward + new Vector3(x, 0, y),Vector3.zero);
            transform.Translate(x, 0, y,Space.Self);
            //Quaternion.LookRotation(transform.position - new Vector3(x, 0, y));
            //transform.LookAt(transform.position + transform.localPosition.normalized);

            //transform.Rotate(0,90,0);

        }
    }

    public void CantMove(bool _canMove)
    {
        canMove = _canMove;
    }

    public bool getCanmove { get { return canMove; } }
}
