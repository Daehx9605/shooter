﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    private Move move;
    private Rotation rotation;
    [SerializeField]
    private float posX;
    [SerializeField]
    private GameObject checkPos;

    private void Awake()
    {
        move = GetComponent<Move>();
        rotation = GetComponent<Rotation>();
    }


    public void Check()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.up, out hit))
        { 
            move.CantMove(true);
            Debug.DrawRay(transform.position, -transform.up, Color.red);
        }
        else
        {
            
            Debug.Log("sale");
            move.CantMove(false);
            rotation.PlayerRotation();
        }
    }

}